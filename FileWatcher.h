#pragma once

#include <algorithm>
#include <chrono>
#include <filesystem>
#include <functional>
#include <string>
#include <thread>
#include <unordered_map>

namespace Sph {

enum class FileStatus { Created, Modified, Deleted, None };

class FileWatcher final {
 public:
  FileWatcher(std::string const& path,
              std::chrono::duration<uint64_t, std::milli> const);

  void Run(std::function<void(std::string const&, FileStatus const)> const&);

 private:
  FileStatus GetFileStatus(std::filesystem::directory_entry const&) const
      noexcept;
  void ProcessEntry(std::filesystem::directory_entry const& entry,
                    FileStatus const);

  std::chrono::duration<uint64_t, std::milli> _delay;
  std::unordered_map<std::string, std::filesystem::file_time_type> _entries;
  std::string _path;
};

}  // namespace Sph
