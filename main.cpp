#include <iostream>

#include "FileWatcher.h"

int main() {
  auto fw = Sph::FileWatcher("./", std::chrono::milliseconds(3000));

  fw.Run([](std::string const& filename, Sph::FileStatus status) {
    auto const status_str = [&]() {
      switch (status) {
        case Sph::FileStatus::Created:
          return "Created";
        case Sph::FileStatus::Modified:
          return "Modified";
        case Sph::FileStatus::Deleted:
          return "Deleted";
        default:
          return "Unknown";
      }
    }();

    std::cout << "Got file: " << filename << " with status: " << status_str
              << '\n';
  });
}
