#include "FileWatcher.h"

Sph::FileWatcher::FileWatcher(
    std::string const& path,
    std::chrono::duration<uint64_t, std::milli> const delay)
    : _path(path), _delay(delay) {
  for (auto const& file : std::filesystem::recursive_directory_iterator(path)) {
    _entries.emplace(file.path().string(),
                     std::filesystem::last_write_time(file));
  }
}

// Run the FileWatcher with an action that executes for every file
void Sph::FileWatcher::Run(
    std::function<void(std::string const&, FileStatus const)> const& action) {
  // Run the infinite loop
  while (true) {
    // Iterate recursively over all the files in the base path
    for (auto const& entry :
         std::filesystem::recursive_directory_iterator(_path)) {
      // We only care about regular files
      if (!entry.is_regular_file()) {
        continue;
      }

      // TODO: find a way to not compute the filename 3 times
      auto const filename = entry.path().string();
      auto status = GetFileStatus(entry);

      ProcessEntry(entry, status);

      // Execute the passed in action
      action(filename, status);
    }

    std::this_thread::sleep_for(_delay);
  }
}

// Process a directory entry, based on its status
void Sph::FileWatcher::ProcessEntry(
    std::filesystem::directory_entry const& entry,
    Sph::FileStatus const status) {
  auto const filename = entry.path().string();

  switch (status) {
    case FileStatus::Created:
      _entries.emplace(filename, entry.last_write_time());
      break;
    case FileStatus::Modified:
      _entries[filename] = entry.last_write_time();
      break;
    case FileStatus::Deleted:
      _entries.erase(filename);
      break;
  }
}

// Determine the status of the current directory_entry
Sph::FileStatus Sph::FileWatcher::GetFileStatus(
    std::filesystem::directory_entry const& entry) const noexcept {
  if (!entry.exists()) {
    return FileStatus::Deleted;
  }

  auto const filename = entry.path().string();

  if (!_entries.contains(filename)) {
    return FileStatus::Created;
  } else if (_entries.at(filename) != std::filesystem::last_write_time(entry)) {
    return FileStatus::Modified;
  } else {
    return FileStatus::None;
  }
}
